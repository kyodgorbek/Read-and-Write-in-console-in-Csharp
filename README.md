# Read-and-Write-in-console-in-Csharp

using System;


namespace IntroductionToCsharp
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Please Enter Your FirstName");

            string FirstName = Console.ReadLine();

            Console.WriteLine("Please Enter Your LastName");

            string LastName = Console.ReadLine();

            Console.WriteLine("Hello (0), (1)", FirstName, LastName);
        }
    }
}



